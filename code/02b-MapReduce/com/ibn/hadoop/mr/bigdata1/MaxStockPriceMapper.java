package com.ibn.hadoop.mr.bigdata1;

import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MaxStockPriceMapper extends Mapper<LongWritable, Text, Text, FloatWritable>{
	
	@Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		// Read the record in input: a comma delimited string of values
		// Record Structure:
		// quarter	stock	date	open	high	low	close	volume
		String record = value.toString();
		String[] fields = record.split(",");
		
		// <key,value>=<stock,high>
		String stock = fields[1];
		Float high = Float.parseFloat(fields[3]);
		
		// Generate key,value pairs for the intermediate file
		context.write(new Text(stock), new FloatWritable(high));
	}
}

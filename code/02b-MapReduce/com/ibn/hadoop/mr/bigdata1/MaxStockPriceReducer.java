package com.ibn.hadoop.mr.bigdata1;

import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MaxStockPriceReducer extends Reducer<Text, FloatWritable, Text, FloatWritable>{
	
	@Override
	public void reduce(Text key, Iterable<FloatWritable> values, Context context) throws IOException, InterruptedException {
		float maxPrice = Float.MIN_VALUE;
		
		// The Reducer will receive a <key, values> pair with the list of high prices from the mapper for the same stock
		for (FloatWritable value: values) {
			// Store the new max if available
			maxPrice = Math.max(maxPrice, value.get());
		}
		
		//Write the result 
		context.write(key, new FloatWritable(maxPrice));
	}

}

package com.ibn.hadoop.mr.bigdata1;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class MaxStockPrice {
	// Driver program that will submit the job to the Hadoop cluster
	
	public static void main(String[] args) throws Exception {
		if (args.length != 2) {
			System.err.println("Usage: MaxStockPrice <input path> <output path>");
			System.exit(-1);
		}
		
		// Instantiate the MapReduce Job
		Job job = Job.getInstance();
		job.setJarByClass(MaxStockPrice.class);
		job.setJobName("MaxStockPrice");
		
		// Set input and output
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		
		// Set the Mapper/Reducer Process
		job.setMapperClass(MaxStockPriceMapper.class);
		job.setReducerClass(MaxStockPriceReducer.class);
		
		// Set Outputs
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(FloatWritable.class);
		
		// Submit Job to the Cluster		
		System.exit(job.waitForCompletion(true) ? 0:1);
	}
}
